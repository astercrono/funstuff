import mergesort, sortvisual

def main():
	visual = sortvisual.SortVisual(500, 500)
	values = visual.generate_values()
		
	mergesort.sort(values)

	history = mergesort.history

	print "\n # of iterations = %s" % len(history)

	for i in range(0, len(history)):
		if i < len(history):
			changes = history[i]
		visual.print_to_screen(i, changes)
		
	visual.wait_for_close()

main()
