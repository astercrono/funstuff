
history = []

def sort(list):
	history.append(list[:])
	_merge_sort(list, 0, len(list))

# break list down, by index, and merge back together, sorted
def _merge_sort(list, start_index, length):
	if length > 1:
		left_start = start_index
		left_length = int(length/2)
		right_start = left_start + left_length
		right_length = length - left_length
		
		_merge_sort(list, left_start, left_length)
		_merge_sort(list, right_start, right_length)
		
		_merge(list, left_start, left_length, right_start, right_length)
		print list
		history.append(list[:])

# merge right into left
def _merge(list, left_start, left_length, right_start, right_length):
	left_end = left_start + left_length
	right_end = right_start + right_length
	
	print "\t merging index (%(right_start)s - %(right_end)s) into index (%(left_start)s - %(left_end)s)" % {"right_start":right_start, "right_end":right_end-1, "left_start":left_start, "left_end":left_end-1}
	
	for ri in range(right_start, right_end):
		r = list[ri]
		
		for li in range(left_start, left_end):
			l = list[li]
			
			if l >= r:
				_move_value(list, r, li, ri)
				left_end = left_end + 1
				right_start = right_start + 1
				break

# move value to destination in list and shift other values over, careful to stay within specific range
def _move_value(list, value, destination_index, source_index):
	print "\t \t moving %(source_index)s to %(destination_index)s" % {"source_index":source_index, "destination_index":destination_index}
	old = list[destination_index]
	list[destination_index] = value
	
	start = destination_index + 1
	for i in range(start, source_index + 1):
		temp = list[i]
		list[i] = old
		old = temp
