import quicksort, sortvisual

def main():
	visual = sortvisual.SortVisual(500, 500)
	values = visual.generate_values()

	quicksort.sort(values)

	history = quicksort.history

	print "\n # of iterations = %s" % len(history)

	for i in range(0, len(history)):
		if i < len(history):
			changes = history[i]
		visual.print_to_screen(i, changes)
	
	visual.wait_for_close()

main()
