import pygame, random

class SortVisual:

	def __init__(self, max_value, max_iterations):
		self.max_value = max_value
		self.max_iterations = max_iterations
		
		self.hues = []
		self.colors = []
		
		self.screen = pygame.display.set_mode((max_value, max_iterations))
		
		self._init_hues()
		self._init_colors()

	def print_to_console(self, iteration, values):
		print "[%(iteration)s] %(values)s" % {"iteration":iteration, "values":values}

	def print_to_screen(self, iteration, values):
		x_coord = 0

		for v in values:
			vblock = pygame.Surface((1,1))
			vblock.fill(self._pick_color(v))
			
			self.screen.blit(vblock, (x_coord, iteration))
			x_coord = x_coord + 1
		
		pygame.display.flip()

	def generate_values(self):
		values = []

		for x in range(0, self.max_value):
			values.append(int(random.uniform(0, self.max_value)))
			
		return values
		
	def generate_values_ordered(self):
		values = []
		
		for x in range(0, self.max_value):
			if x <= self.max_value:
				values.append(x)
			else:
				break
		
		return values
		
	def wait_for_close(self):
		while True:
			pygame.display.flip()
			
			events = pygame.event.get()
			for event in events:
				if event.type == pygame.QUIT:
					return

	def quit(self):
		pygame.quit()
					
	def _pick_color(self, value):
		return self.colors[value]

	def _init_hues(self):
		r = 255
		g = 0
		b = 0

		for x in range(0, 1531):	
			if x >= 1 and x <= 255:
				g = g + 1
			elif x >= 256 and x <= 510:
				r = r - 1
			elif x >= 511 and x <= 765:
				b = b + 1
			elif x >= 766 and x <= 1020:
				g = g - 1
			elif x >= 1021 and x <= 1275:
				r = r + 1
			elif x >= 1276 and x <= 1530:
				b = b - 1
			
			print "x = %(x)s, r = %(r)s, g = %(g)s, b = %(b)s" % {"r":r, "g":g, "b":b, "x":x}
			self.hues.append(pygame.Color(r, g, b))
			x = x + 1

	def _init_colors(self):
		step = int(len(self.hues)/self.max_value)
		
		x = 0
		while x <= len(self.hues):
			self.colors.append(self.hues[x])
			x = x + step
