
history = []

def sort(list):
	history.append(list[:])
	_quicksort(list, 0, len(list))
	
# Sort by recursively breaking list up by a "pivot", where left is < pivot and right is > pivot.
def _quicksort(list, start_index, length):
	if length == 2:
		item1 = list[start_index]
		item2 = list[start_index + 1]

		if item1 > item2:
			_swap_items(list, start_index, start_index + 1)
	elif length > 2:
		pivot_index = _pick_pivot_index(list, start_index, length)
		
		new_pivot_index = _move_items(list, start_index, length, pivot_index)
		history.append(list[:])

		if new_pivot_index == start_index:
			_quicksort(list, start_index + 1, length - 1)
		elif new_pivot_index == start_index + length - 1:
			_quicksort(list, start_index, length -1)
		else:
			_quicksort(list, start_index, new_pivot_index - start_index)
			_quicksort(list, new_pivot_index + 1, start_index + length - 1 - new_pivot_index)

# Moves pivot new location on list that is based on the numer of items less than it.
#
# If the pivot has not been moved to the beginning or end of the list, move the items
# in the list such that everything < the pivot is on the left and everything greater than
# the pivot is on the right.
#
# Return the pivot's new index.
def _move_items(list, start_index, length, pivot_index):
	pivot = list[pivot_index]
	leftside_count = 0
	
	for i in range(start_index, start_index + length):
		if i != pivot_index and list[i] <= pivot:
			leftside_count += 1
	
	if leftside_count == length - 1:
		_swap_items(list, pivot_index, start_index + length - 1)
		return start_index + length - 1
	elif leftside_count == 0:
		_swap_items(list, pivot_index, start_index)
		return start_index
	else:
		new_pivot_index = start_index + leftside_count
		_swap_items(list, pivot_index, new_pivot_index)
		
		for i in range(start_index, new_pivot_index):
			lv = list[i]
			if lv > pivot:
				for j in range(new_pivot_index + 1, start_index + length):
					rv = list[j]

					if rv <= pivot:
						_swap_items(list, i, j)
						break

		return new_pivot_index

# Uses median of three.
def _pick_pivot_index(list, start_index, length):
	first = list[start_index]
	middle = list[int(length/2) + start_index]
	last = list[start_index + length - 1]
	
	pivot_index = start_index

	if ( (first > middle and first < last) or (first < middle and first > last) ):
		return start_index
	elif ( (middle > first and middle < last) or (middle < first and middle > last) ): 
		return int(length/2) + start_index
	else:
		start_index + length - 1
	
	return pivot_index

def _swap_items(list, i1, i2):
	if i1 != i2:
		temp = list[i1]

		list[i1] = list[i2]
		list[i2] = temp

