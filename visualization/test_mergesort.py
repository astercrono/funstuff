import mergesort, random, time
		
values = []
for x in range(0, 100):
	values.append(int(random.uniform(0, 100)))
	
print values
	
original = values[:]
original.sort()

print "Sorting..."

ts = time.time()*1000
mergesort.sort(values)
te = time.time()*1000

success = original = values
if success:
	print "\n success (%(duration)s ms)" % {"duration":te - ts}
else:
	print "failed"
