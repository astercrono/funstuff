This repository contains random projects that I have developed for fun.

At the moment, they are all in Python as part of my process for learning Python.

There are two projects, for lack of a better term and running them is simple enough.

* braingle/ClosedDoors
* * This is an implementation of a problem I stumble into on braingle.com. Be warned, this is a very messy project that I whipped together very quickly.
* visualation
* * A suite of sorting algorithms visualized with color hues. modules prefixed with "test_" can be run without parameters.

