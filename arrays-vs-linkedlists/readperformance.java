package tkern.testing.performance;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ArraysVsLinkedLists
{	
	public static void main(String[] args)
	{
		final int seed = 32;
		final int numOfElements = 10000000;
		
		final List<Integer> arrayList = new ArrayList<Integer>(numOfElements);
		final List<Integer> linkedList = new LinkedList<Integer>();
		
		final Random randomGen = new Random(seed);
		
		for (int i = 0; i < numOfElements; i++)
		{
			Integer nextInt = randomGen.nextInt();
			arrayList.add(i);
			linkedList.add(nextInt);
		}
		
		long arrayStart = System.currentTimeMillis();
		for (Integer i : arrayList)
		{
			;
		}
		long arrayEnd = System.currentTimeMillis();
		System.out.println("array time = " + (arrayEnd - arrayStart));
		
		flushCache(numOfElements, seed);
		
		long linkedListStart = System.currentTimeMillis();
		for (Integer i : linkedList)
		{
			;
		}
		long linkedListEnd = System.currentTimeMillis();
		System.out.println("linked list time = " + (linkedListEnd - linkedListStart));
	}
	
	// I am not sure if I need this.
	// My idea was that after running through the arraylist, i should be completely certain that nothing is cached.
	private static void flushCache(int numOfElements, int seed)
	{
		List<Integer> arrayList = new ArrayList<Integer>(numOfElements);
		final Random randomGen2 = new Random(seed + 1);
		for (int i = 0; i < numOfElements; i++)
		{
			Integer nextInt = randomGen2.nextInt();
			arrayList.add(i);
		}
		for (Integer i : arrayList)
		{
			;
		}
	}
}

