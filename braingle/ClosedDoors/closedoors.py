import pygame, time, pdb

CLOSED_COLOR = pygame.Color("#207CE5") # blue
OPEN_COLOR = pygame.Color("#40B04E") # green
NUM_OF_DOORS = 500

doors = []
history = []

def toggle(doors, index):
	if doors[index] == 0:
		doors[index] = 1
	else:
		doors[index] = 0

for x in range(NUM_OF_DOORS):
	doors.append(1)

history.append(doors[:])

print "calculating doors"

for n in range(2, NUM_OF_DOORS+1):
	for x in range(1, NUM_OF_DOORS+1):
		if x % n == 0:
			toggle(doors, x-1)
	history.append(doors[:])
	
total_open = 0
total_closed = 0

print "counting final open/closed"

for x in range(NUM_OF_DOORS):
	d = doors[x]
	
	if d == 0:
		total_closed = total_closed + 1
	else:
		total_open = total_open + 1

screen = pygame.display.set_mode((NUM_OF_DOORS, NUM_OF_DOORS))

def pick_color(door):
	if door == 0:
		return CLOSED_COLOR
	else:
		return OPEN_COLOR

x_coord = 0
y_coord = 0

print "drawing %(num_of_doors)s doors through %(iterations)s iterations" % {"num_of_doors": NUM_OF_DOORS, "iterations":len(history)}

for y in range(NUM_OF_DOORS):
	h = history[y]
	print "[%(y)s] %(doors)s" % {"y":y+1, "doors":h}

	for x in range(NUM_OF_DOORS):
		d = h[x]
		door_surface = pygame.Surface((1,1))
		door_surface.fill(pick_color(d))
		
		screen.blit(door_surface, (x_coord, y_coord))
		pygame.display.flip()
		x_coord = x_coord + 1
	x_coord = 0
	y_coord = y_coord + 1

running = True
try:
    while running:
	pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
    pygame.quit()
except SystemExit:
    pygame.quit()
